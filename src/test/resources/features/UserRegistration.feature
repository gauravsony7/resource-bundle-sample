Feature: Multi-Language Verification

  Scenario Outline: Verify that description message for all language

    Given navigate to application with "<Language>" language
    And click on about us menu
    And select out laboratory sub option
    And verify title for the lab in the middle of page
    And verify description for the lab

    Examples:
      | Language |
      | English  |