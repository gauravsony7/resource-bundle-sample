package pmc.steps;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.testng.Assert;
import pmc.pages.Home;
import pmc.runner.TestRunner;

public class HomeSteps {


    String lab_header_It = "IL NOSTRO LABORATORIO";
    String lab_desc_It = "Con il nostro laboratorio, i prodotti Dream Glass Group sono dotati delle più recenti e migliori tecnologie in fatto di vetri per la privacy e sono testati per soddisfare i più elevati standard di qualità.";


    String lab_header_En = "OUR LABORATORY";
    String lab_desc_En = "With our own laboratory, Dream Glass Group products are equipped with the latest and greatest in privacy glass technology, and are tested to meet the highest quality standards.";

    Home home;

    public HomeSteps() {
        home = new Home(TestRunner.dm.getDriver());
    }

    @Given("^navigate to application with \"([^\"]*)\" language$")
    public void navigateToApplicationWithLanguage(String arg0) throws Throwable {

    }

    @And("^click on about us menu$")
    public void clickOnAboutUsMenu() {
        home.clickOnAboutMenu();
    }

    @And("^select out laboratory sub option$")
    public void selectOutLaboratorySubOption() {
        home.clickOnOurLab();
    }

    @And("^verify title for the lab in the middle of page$")
    public void verifyTitleForTheLabInTheMiddleOfPage() {
        String Act_title = home.getLabTitle();
        String exp_title = "";
        if (TestRunner.url.contains("/it/")) {
            exp_title = lab_header_It;
        } else {
            exp_title = lab_header_En;
        }
        Assert.assertEquals(Act_title, exp_title, "Lab title does not match");
    }

    @And("^verify description for the lab$")
    public void verifyDescriptionForTheLab() {
        String act_des = home.getLabDescription();
        String exp_des = "";

        if (TestRunner.url.contains("/it/")) {
            exp_des = lab_desc_It;
        } else {
            exp_des = lab_desc_En;
        }

        Assert.assertEquals(act_des, exp_des, "Lab description does not match");
    }

}