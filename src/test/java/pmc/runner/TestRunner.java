package pmc.runner;

import com.pmc.cucumber.HTMLReportGenerator;
import com.pmc.cucumber.ScenarioInstance;
import pmc.pages.Home;
import com.pmc.selenium.BrowserInitiationHelper;
import com.pmc.selenium.BrowserNavigationHelper;
import com.pmc.selenium.DriverManager;
import com.pmc.utils.PropertyUtil;
import com.pmc.utils.ScreenshotUtil;
import com.pmc.utils.SystemUtil;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.*;

import java.io.IOException;
import java.net.MalformedURLException;


@CucumberOptions(
        plugin = {"pretty", "json:target/report/cucumber.json"},
        monochrome = true,
        features = {"src/test/resources/features"},
        glue = {"pmc.steps",
                "pmc.runner"},
        dryRun = false,
        tags = "")

public class TestRunner extends AbstractTestNGCucumberTests {

    private static final Logger logger = LogManager.getLogger(TestRunner.class.getName());
    public static PropertyUtil envProp = new PropertyUtil(SystemUtil.getUserDirectory()+"/configuration/config.properties");
    public static String url;
    
    public static DriverManager dm = new DriverManager();
    public static ScenarioInstance scI = new ScenarioInstance();
    
    /**
     * To Initialize browser and load application Url
     */
    @BeforeTest
    public void initializeProperties() {

    }

    @Override
    @DataProvider(parallel = true)
    public Object[][] scenarios() {
        return super.scenarios();
    }
    
    /**
     * To generate reports after execution
     */
    @BeforeSuite
    public void beforeSuite() {
        url=envProp.getProperty("app-url");
        logger.info("----------Initializing browser---------");
    }

    /**
     * This method will be called before each scenario, it will initialise the scenario object
     *
     * @param
     */
    @Before(order = 0)
    public void beforeScenario(Scenario scenario) throws MalformedURLException {

        dm.setDriver(new BrowserInitiationHelper().getBrowser(envProp.getProperty("web-browser")));
        scI.setScenario(scenario);
        new BrowserNavigationHelper(dm.getDriver()).loadUrl(url);
        new Home(dm.getDriver()).acceptCookies();
    }

    /**
     * This method will be called after each scenario, it will take screen shot
     * if test case is failed
     */
    @After(order = 0)
    public void afterScenario(Scenario scenario) throws IOException {
        takeScreenShotOnFailure();
        dm.getDriver().quit();
    }

    /**
     * To generate reports after execution
     */
    @AfterSuite
    public void afterSuite() {
        logger.info("----------Generating cucumber report----------");
        HTMLReportGenerator.generateReport();
    }

    /**
     * This method will be called after each scenario, it will take screen shot
     * if test case is failed
     */
    private void takeScreenShotOnFailure() throws IOException {
        if (scI.getScenario().isFailed()) {
            ScreenshotUtil.captureScreenshot(dm.getDriver(), scI.getScenario());
            logger.error("!!!!!!!!!!!!! Scenario- " + scI.getScenario().getName() + " Failed !!!!!!!!!!!!!!!!!");
        } else {
            logger.info("************* Scenario- " + scI.getScenario().getName() + " Passed *****************");
        }
    }

    /**
     * To perform activities after execution
     */
    @AfterTest(alwaysRun = true)
    public void tearDown() {
    }
    
}