package pmc.pages;

import com.pmc.selenium.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class BasePage {
    private  final Logger logger = LogManager.getLogger(BasePage.class.getName());
    public WebDriver driver;
    public final int waitTime = 30;

    public WaitHelper waitHelper;
    public ActionsHelper actionsHelper;
    public ElementHelper eleHelper;
    public BrowserNavigationHelper browsNav;
    public JSInteractionHelper jSHelper;
    public FramesAlertsHelper alertsHelper;
    public ScrollHelper scrollHelper;

     public BasePage(WebDriver driver) {
         this.driver =driver;
         waitHelper= new WaitHelper(driver,waitTime);
         actionsHelper= new ActionsHelper(driver);
         eleHelper= new ElementHelper(driver);
         browsNav=new BrowserNavigationHelper(driver);
         scrollHelper= new ScrollHelper(driver);
         jSHelper= new JSInteractionHelper(driver);
         alertsHelper=new FramesAlertsHelper(driver);
         PageFactory.initElements(driver, this);
    }
}