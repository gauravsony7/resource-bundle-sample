package pmc.pages;

import com.pmc.selenium.WaitHelper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Home extends BasePage {
    private  final Logger logger = LogManager.getLogger(Home.class.getName());


    @FindBy(id = "menu-item-49")
    protected WebElement menuAboutUs;

    @FindBy(id = "menu-item-1424")
    protected WebElement menuOptionLab;

    @FindBy(xpath = "//*[@class=\"header-text-sectors\"]")
    protected WebElement labDescription;

    @FindBy(xpath = "//h2[@class=\"header-title-sectors\"]")
    protected WebElement labTitle;

    @FindBy(css = "#catapultCookie")
    public WebElement AcceptCookies;



    public Home(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }


    public void acceptCookies()
    {
        waitHelper.waitUntilVisibilityOf(AcceptCookies,30);
        jSHelper.clickWithJS(AcceptCookies);
        waitHelper.waitUntilDisappear(AcceptCookies,waitTime);
        WaitHelper.timeout(3000);
    }

    public void clickOnAboutMenu() {
        scrollHelper.scrollTop();
        WaitHelper.timeout(500);
        waitHelper.waitUntilVisibilityOf(menuAboutUs,10);
        actionsHelper.moveToElement(menuAboutUs);
        waitHelper.waitUntilVisibilityOf(menuOptionLab,waitTime);
    }

    public void clickOnOurLab() {
        eleHelper.clickElement(menuOptionLab);
        waitHelper.waitUntilVisibilityOf(labDescription,waitTime);
    }

    public String getLabDescription() {
        waitHelper.waitUntilVisibilityOf(labDescription,waitTime);
        return eleHelper.getElementText(labDescription);
    }

    public String getLabTitle() {
        waitHelper.waitUntilVisibilityOf(labTitle,waitTime);
        return eleHelper.getElementText(labTitle);
    }



}