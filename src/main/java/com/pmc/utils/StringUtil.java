package com.pmc.utils;

import org.apache.commons.lang.WordUtils;

import java.util.Arrays;

public class StringUtil {
    /**
     * To check if the string is Number or not
     *
     * @param strNum String
     * @return boolean <p>True if number, false otherwise</p>
     */
    private static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /***
     * Converts Key to Pascal Case and Remove all Special Characters
     * @param text String <p>Text to be formatted</p>
     * @return
     */
    public static String getFormattedKey(String text) {
        String pascalCase = WordUtils.capitalizeFully(text).trim();
        pascalCase = pascalCase.replaceAll("[\\s()]+", "");
        pascalCase = pascalCase.replaceAll("[^A-Za-z0-9]", "");
        return pascalCase.trim();
    }

    /***
     * To Convert CamelCase String to Sentence Case. i.e. IAmCamelCase --> I Am Camel Case
     * @param stringInCamelCase String
     * @return Sentence Case String
     */
    private static String splitCamelCase(String stringInCamelCase) {
        return stringInCamelCase.replaceAll(
                String.format("%s|%s|%s",
                        "(?<=[A-Z])(?=[A-Z][a-z])",
                        "(?<=[^A-Z])(?=[A-Z])",
                        "(?<=[A-Za-z])(?=[^A-Za-z])"
                ),
                " "
        );
    }

    /**
     * To check if the Number is Integer or Double
     *
     * @param number Double
     * @return true if Integer, false otherwise
     */
    public static boolean isInteger(double number) {
        return Math.ceil(number) == Math.floor(number);
    }

    /***
     * To check if the String is Boolean or not
     * @param value String
     * @return True if boolean, false otherwise
     */
    private boolean isBoolean(String value) {
        value.trim();
        return Arrays.stream(new String[]{"true", "false", "1", "0"})
                        .anyMatch(b -> b.equalsIgnoreCase(value));
    }
}
