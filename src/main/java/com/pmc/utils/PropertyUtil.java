package com.pmc.utils;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyUtil {
    private  final Logger logger = LogManager.getLogger(PropertyUtil.class.getName());
    private String CONFIG_PATH;
    public  Properties prop;
    public  FileInputStream fileInputStream;
    //  variable single_instance of type Singleton
    private  PropertyUtil prop_instance = null;

    public PropertyUtil(String filePath) {
        this.CONFIG_PATH=filePath;

        prop = new Properties();
        try {
            fileInputStream = new FileInputStream(new File(filePath));
            prop.load(fileInputStream);
        }  catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


        /**
     * This method will return value from property file
     *
     * @param key String <p>key for which value needs to be fetched</p>
     * @return String <p>Value for the given key as an input</p>
     */
    public String getProperty(String key) {
        String returnProperty = prop.getProperty(key);
        try {
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnProperty;
    }

    /**
     * This method will update value of specified key in property file
     *
     * @param key   String <p>Key for which value needs to be updated</p>
     * @param value String <p>Value to be updated with</p>
     */

    public void updateProperty(String key, String value) {
        try {
            PropertiesConfiguration conf = new PropertiesConfiguration(CONFIG_PATH);
            conf.setProperty(key, value);
            conf.save();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}