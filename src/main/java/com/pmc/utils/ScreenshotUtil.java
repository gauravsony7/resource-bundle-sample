package com.pmc.utils;


import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import java.io.File;
import java.io.IOException;

public class ScreenshotUtil {
    private static final Logger logger = LogManager.getLogger(ScreenshotUtil.class.getName());

    /**
     * Below method captures screenshot on failure of any scenario
     *
     * @param scenario <p>Current scenario instance</p>
     */
    public static void captureScreenshot(WebDriver driver, Scenario scenario) {
        captureScreenshot(driver,scenario,"");
    }

    public static void captureScreenshot(WebDriver driver, Scenario scenario, String name) {
        final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        scenario.attach(screenshot, "image/png", name); //
    }




    /**
     * Below method captures screenshot of an element
     *
     * @param by       By <p>locator of an element</p>
     * @param filePath String <p>File path in current directory</p>
     * @throws IOException
     */
    public static void getElementScreenshot(WebDriver driver,By by, String filePath) throws IOException {
        WebElement element = driver.findElement(by);
        File scrFile = element.getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(SystemUtil.getUserDirectory() + filePath));
    }
}
