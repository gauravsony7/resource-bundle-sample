package com.pmc.utils;

public class SystemUtil {

    /**
     * Below method gets us the current user directory
     *
     * @return User working directory
     */
    public static String getUserDirectory() {
        return System.getProperty("user.dir");
    }

    /**
     * Below method gets us the file separator for the current OS
     *
     * @return Character that separates components of a file path. This is "/" on UNIX and "\" on Windows.
     */
    public static String getFileSeparator() {
        return System.getProperty("file.separator");
    }

    /**
     * Below method gets us the current OS name
     *
     * @return Operating system name
     */
    public static String getOperatingSystemName() {
        return System.getProperty("os.name");
    }

    /**
     * Below method gets us the current OS architecture
     *
     * @return Operating system architecture
     */
    public static String getOperatingSystemArchitecture() {
        return System.getProperty("os.arch");
    }

    /**
     * Below method gets us the current OS version
     *
     * @return Operating system version
     */
    public static String getOperatingSystemVersion() {
        return System.getProperty("os.version");
    }

    /**
     * Below method gets us the path separator
     *
     * @return Path separator character used in java.class.path
     */
    public static String getPathSeparator() {
        return System.getProperty("path.separator");
    }

    /**
     * Below method gets us the current OS name
     *
     * @return User home directory
     */
    public static String getUserHomeDirectory() {
        return System.getProperty("user.home");
    }

    /**
     * Below method gets us the current logged in account name
     *
     * @return User account name
     */
    public static String getUserAccountName() {
        return System.getProperty("user.name");
    }

    /**
     * Below method gets us the Line Separator
     *
     * @return Sequence used by operating system to separate lines in text files
     */
    public static String getLineSeparator() {
        return System.getProperty("line.separator");
    }

    /**
     * Below method gets us the default temporary file path
     *
     * @return Default temp file path
     */
    public static String getDefaultTempFilePath() {
        return System.getProperty("java.io.tmpdir");
    }
}