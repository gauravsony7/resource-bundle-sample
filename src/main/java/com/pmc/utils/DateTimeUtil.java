package com.pmc.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeUtil {

    /**
     * To get the current date time with format provided as per user
     *
     * @param format String <p>Date Time format</p>
     * @return String <p>Formatted time stamp as per the given format</p>
     */
    public static String getCurrentTimeStampWithFormatAs(String format) {
        return new SimpleDateFormat(format).format(new Date());
    }

    /**
     * To get the current date time with format dd-MM-yyyy_HH.mm.ss
     *
     * @return String <p>Formatted time stamp as per the above format</p>
     */
    public static String getCurrentTimeStamp() {
        return new SimpleDateFormat("dd-MM-yyyy_HH.mm.ss").format(new Date());
    }

    /**
     * Below method gets the current date in dd-MM-yyyy format
     *
     * @return String <p>Formatted current date</p>
     */
    public static String getCurrentDate() {
        return new SimpleDateFormat("dd-MM-yyyy").format(new Date());
    }

    /**
     * Below method gets the date in specific given format
     *
     * @param dateFormat <p>Format type to convert today's date to</p>
     * @return String <p>Date in given format</p>
     */
    public static String getCurrentDate(String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(new Date());
    }

    /**
     * Below method gets the current time in HH:mm:ss format
     *
     * @return String <p>Formatted current time</p>
     */
    public static String getCurrentTime() {
        return new SimpleDateFormat("HH:mm:ss").format(new Date());
    }

    /**
     * Below method gets the time in specific given format
     *
     * @param timeFormat <p>Format type to convert current time to</p>
     * @return String <p>Time in given format</p>
     */
    public static String getCurrentTime(String timeFormat) {
        return new SimpleDateFormat(timeFormat).format(new Date());
    }

    /**
     * To get the given date formatted as per the provided format
     *
     * @param date       Date <p>Date object provided as an input</p>
     * @param dateFormat String <p>Date format provided to convert date</p>
     * @return String <p>Formatted date as per the given format</p>
     */
    public static String getDateFormatted(Date date, String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(date);
    }

    /**
     * To get current time stamp in IST format
     *
     * @return String <p>Formatted IST time stamp</p>
     */
    public static String getCurrentTimeStampIST() {
        TimeZone istTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
        Calendar today = Calendar.getInstance(istTimeZone);
        SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a z");
        return FORMATTER.format(today.getTime()).replaceAll(" ","_");
    }
}
