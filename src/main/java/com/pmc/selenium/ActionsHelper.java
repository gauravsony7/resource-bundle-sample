package com.pmc.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 * Below class contains all the methods to interact elements using Actions class
 */
public class ActionsHelper {
    private  final Logger logger = LogManager.getLogger(ActionsHelper.class.getName());
    WebDriver driver;
    JSInteractionHelper jsHelp;
    WaitHelper waitHelper;

    public ActionsHelper(WebDriver driver) {
        this.driver=driver;
        jsHelp= new JSInteractionHelper(driver);
        waitHelper= new WaitHelper(driver);
    }

    /**
     * Move to element using Actions class
     *
     * @param element WebElement <p>Locator of the element</p>
     */
    public void moveToElement(WebElement element) {
        moveToElement(element,"Green");
    }

    public  void moveToElement(WebElement element,String highColor) {
        jsHelp.highLightElement(element,highColor);
        new Actions(driver).moveToElement(element).build().perform();
    }

    /**
     * Move to element using Actions class
     *
     * @param by By <p>Locator of the element</p>
     */
    public  void moveToElement(By by) {
            WebElement element = driver.findElement(by);
            jsHelp.highLightElement(element, "Green");
            new Actions(driver).moveToElement(element).build().perform();
    }

    /**
     * Move to element and click using Actions class
     *
     * @param element WebElement <p>Locator of the element</p>
     */
    public  void moveToElementAndCLick(WebElement element) {
            Actions action = new Actions(driver);
            waitHelper.waitUntilElementClickable(element);
            moveToElement(element);
            action.click(element).build().perform();
    }

    /**
     * Move to element and click using Actions class
     *
     * @param element WebElement <p>Locator of the element</p>
     */
    public  void moveToElementAndRightCLick(WebElement element) {
            Actions action = new Actions(driver);
            waitHelper.waitUntilElementClickable(element);
            moveToElement(element);
            action.contextClick(element).build().perform();
    }

//    public  void clickOnImage(String imagePath) throws FindFailed {
//        Screen screen = new Screen();
//        Match match = screen.wait(new Pattern(imagePath), wait);
//        match.click();
//    }

    /**
     * Move to element and click using Actions class
     *
     * @param by By <p>Locator of the element</p>
     */
    public  void moveToElementAndCLick(By by) {
        Actions action = new Actions(driver);
        WebElement element = driver.findElement(by);
        waitHelper.waitUntilElementClickable(element);
        moveToElement(element);
        action.click(element).build().perform();
    }
}