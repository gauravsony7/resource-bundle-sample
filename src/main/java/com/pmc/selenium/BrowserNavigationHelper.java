package com.pmc.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Below method contains all the methods for Browser navigation and Frame handling
 */
public class BrowserNavigationHelper {

    WebDriver driver;
    public BrowserNavigationHelper(WebDriver driver)
    {
        this.driver=driver;
    }

    private Logger logger = LogManager.getLogger(BrowserNavigationHelper.class.getName());

    /**
     * Below method loads the url in web-browser
     *
     * @param url String <p>URL of the website</p>
     */
    public  void loadUrl(String url) {
        driver.manage().timeouts().pageLoadTimeout(2000, TimeUnit.SECONDS);
        driver.get(url);
    }

    /**
     * Below method gets the title of the current tab
     *
     * @return String <p>Current tab title</p>
     */
    public  String getCurrentTitle() {
        logger.info("Current tab title: " + driver.getTitle());
        return driver.getTitle();
    }

    /**
     * Below method gets the current url of the tab
     *
     * @return String <p>Current URL</p>
     */
    public  String getCurrentUrl() {
        logger.info("Current tab URL: " + driver.getCurrentUrl());
        return driver.getCurrentUrl();
    }

    /**
     * Below method helps navigating back in browser
     */
    public  void navigateBack() {
        driver.navigate().back();
        logger.info("Navigated back");
    }

    /**
     * Below method helps navigating forward in browser
     */
    public  void navigateForward() {
        driver.navigate().forward();
        logger.info("Navigated forward");
    }

    /**
     * Below method helps refreshing the browser
     */
    public  void navigateRefresh() {
        driver.navigate().refresh();
        logger.info("Navigated refresh");
    }

    /**
     * Below method helps switching to new window on click of an element
     */
    public  void switchToWindow(By by, String newWindowTitle) {
        String originalWindow = driver.getWindowHandle();
        assert driver.getWindowHandles().size() == 1;
        driver.findElement(by).click();
        //wait.until(numberOfWindowsToBe(2));
        for (String windowHandle : driver.getWindowHandles()) {
            if (!originalWindow.contentEquals(windowHandle)) {
                driver.switchTo().window(windowHandle);
                break;
            }
        }
        //wait.until(titleIs(newWindowTitle));
    }

    /**
     * Below method closes the tab or window
     */
    public  void closeTaborWindow() {
        driver.close();
        logger.info("Closed tab or window");
    }

    /**
     * Below method gets the window size in dimension
     *
     * @return Dimension <p>Dimension object</p>
     */
    public  Dimension getWindowSize() {
        return driver.manage().window().getSize();
    }

    /**
     * This method will switch to new tab
     *
     * @param i
     */
    public  void switchNewTab(int i) {
        ArrayList<String> tabs = new ArrayList<>(
                driver.getWindowHandles());
        driver.switchTo().window(tabs.get(i));
    }

    /**
     * This method will open a new tab in existing window
     */
    public  void openANewTab(){
        ((JavascriptExecutor)driver).executeScript("window.open()");
    }

    public  void switchToFirstTab() {

        ArrayList<String> tabs = new ArrayList<String> ( driver.getWindowHandles());

        try
        {
            for (int i=1;i<tabs.size ();i++)
            {
                try {  driver.switchTo().window(tabs.get(i)).close(); }
                catch (org.openqa.selenium.NoSuchWindowException e){ }
            }

            driver.switchTo().window(tabs.get(0));
        }
        catch (Exception e){}
    }
}
