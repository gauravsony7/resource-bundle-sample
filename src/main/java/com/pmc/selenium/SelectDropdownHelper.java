package com.pmc.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SelectDropdownHelper {
    private  final Logger logger = LogManager.getLogger(SelectDropdownHelper.class.getName());

    /**
     * Below method selects a dropdown option by text
     *
     * @param element WebElement <p>Locator of dropdown</p>
     * @param text    <p>Text to select from dropdown select</p>
     */
    public static void selectByText(WebElement element, String text) {
            element.click();
            WaitHelper.timeout(200);
            new Select(element).selectByVisibleText(text);
    }

    /**
     * Below method selects dropdown option by value
     *
     * @param element WebElement <p>Locator of dropdown</p>
     * @param value   <p>Value to select from Options</p>
     */
    public static void selectByValue(WebElement element, String value) {
            new Select(element).selectByValue(value);
    }

    /**
     * Below method selects dropdown option by index
     *
     * @param element WebElement <p>Locator of dropdown</p>
     * @param index   <p>index to select from dropdown</p>
     */
    public  void selectByIndex(WebElement element, int index) {
            new Select(element).selectByIndex(index);
    }
}