package com.pmc.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class FramesAlertsHelper {
    private  final Logger logger = LogManager.getLogger(FramesAlertsHelper.class.getName());

    WebDriver driver;
    WaitHelper waitHelper;
    public FramesAlertsHelper(WebDriver driver)
    {
        this.driver=driver;
        waitHelper=new WaitHelper(driver);
    }

    /**
     * Below method waits for alert and accepts a simple alert which shows a custom message
     *
     * @return String <p>Text present on alert</p>
     */
    public String handleAcceptAlert() {
        String alert_text = "";
        try {
            waitHelper.waitUntilAlertPresence();
            Alert alert =driver.switchTo().alert();
            alert_text = alert.getText();
            alert.accept();
        } catch (Exception ex) {
            logger.info("Error in accepting OK alert" + ex.getMessage());
        }
        return alert_text;
    }

    /**
     * Below method waits for alert and gets the text and dismiss a confirm alert
     *
     * @return String <p>Text present on alert</p>
     */
    public  String handleConfirmAlert() {
        String alert_text = null;
        try {
            waitHelper.waitUntilAlertPresence();
            Alert alert =driver.switchTo().alert();
            alert_text = alert.getText();
            alert.dismiss();
        } catch (Exception ex) {
            logger.info("Error in accepting alert" + ex.getMessage());
        }
        return alert_text;
    }

    /**
     * Below method waits for alert and handles prompt alert
     *
     * @param textToBeEntered String <p>Text present on alert</p>
     */
    public  void handlePromptAlert(String textToBeEntered) {
        try {
            waitHelper.waitUntilAlertPresence();
            Alert alert =driver.switchTo().alert();
            alert.sendKeys(textToBeEntered);
            alert.accept();
        } catch (Exception ex) {
            logger.info("Error in accepting alert" + ex.getMessage());
        }
    }

    /**
     * Below method helps Switch to frame with locator
     *
     * @param by By <p>locator of the frame</p>
     */
    public  void switchToFrame(By by) {
        WebElement element =driver.findElement(by);
       driver.switchTo().frame(element);
        logger.info("Switched to frame: " + element);
    }

    /**
     * Below method helps Switch to frame with frame number
     *
     * @param frameNumber int <p>Frame number</p>
     */
    public  void switchToFrame(int frameNumber) {
       driver.switchTo().frame(frameNumber);
        logger.info("Switched to frame number: " + frameNumber);
    }

    /**
     * Below method helps Switch to frame with frame name or ID
     *
     * @param frameAttribute int <p>Frame name or Frame ID</p>
     */
    public  void switchToFrame(String frameAttribute) {
       driver.switchTo().frame(frameAttribute);
        logger.info("Switched to frame with attribute: " + frameAttribute);
    }

    /**
     * Below method helps Switch to default level
     */
    public  void switchToDefaultLevel() {
       driver.switchTo().defaultContent();
        logger.info("Switched to default content");
    }
}