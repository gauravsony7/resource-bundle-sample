package com.pmc.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ScrollHelper {
    private  final Logger logger = LogManager.getLogger(ScrollHelper.class.getName());

    WebDriver driver;
    public ScrollHelper(WebDriver driver)
    {
        this.driver=driver;
    }

    /**
     * To scroll to a specific element in dom
     *
     * @param by <p>Locator to scroll to</p>
     */
    public  void scrollToElement(By by) {
        try {
            WebElement element = driver.findElement(by);
            scrollToElement(element);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * To scroll to a specific element in dom
     *
     * @param element <p>Locator to scroll to</p>
     */
    public  void scrollToElement(WebElement element) {
        try {
            JavascriptExecutor jse = ((JavascriptExecutor) driver);
            jse.executeScript("arguments[0].scrollIntoView(false);", element);
            Thread.sleep(500);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * To scroll down to a specific element in dom
     */
    public  void scrollToBottom() {
        try {
            JavascriptExecutor jse = ((JavascriptExecutor) driver);
            jse.executeScript("window.scrollBy(0,700)");
            Thread.sleep(500);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * To scroll to top of the page
     */
    public  void scrollTop() {
        try {
            WebElement element = driver.findElement(By.tagName("header"));
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("arguments[0].scrollIntoView();", element);
            Thread.sleep(500);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
