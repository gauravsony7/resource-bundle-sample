package com.pmc.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class WaitHelper {
    private static Logger logger = LogManager.getLogger(WaitHelper.class.getName());

    public static int waitTime;
    WebDriver driver;
    public WaitHelper(WebDriver driver,int defWaitTime)
    {
        this.driver=driver;
        this.waitTime=defWaitTime;
    }
    public WaitHelper(WebDriver driver)
    {
        this.driver=driver;
    }

    /**
     * This method will hard wait for given time
     *
     * @param milliseconds int <p>Time to wait for</p>
     */
    public static void timeout(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException ex) {
            logger.error("Error in hard-wait" + ex);
        }
    }
    public void waitPageTitleTobe(String pageTitle,int seconds) {
            new WebDriverWait(driver, seconds).until(ExpectedConditions.titleContains(pageTitle));
    }

    /**
     * Wait until element is visible
     *
     * @param locator WebElement <p>Locator to wait for</p>
     * @param seconds int <p>Number of seconds to wait for</p>
     */
    public  void waitUntilVisibilityOf(WebElement locator, int seconds) {
        try {
            logger.info("Waiting for the visibility of element : " + locator);
            new WebDriverWait(driver, seconds)
                    .until(ExpectedConditions.visibilityOf(locator));
        } catch (Exception e) {
            logger.error("Error in visibility of an element" + e.getMessage());
        }
    }


    /**
     * Wait until element is visible in DOM
     *
     * @param locator, Seconds
     */
    public  WebElement waitUntilVisibilityOf(By locator, int seconds) {
        try {
            logger.info("Waiting for " + locator);
            WebDriverWait wait = new WebDriverWait(driver, (seconds / 2));
            wait.until(ExpectedConditions.presenceOfElementLocated(locator));
            WebDriverWait waitV = new WebDriverWait(driver, (seconds / 2));
            WebElement el = driver.findElement(locator);
            waitV.until(ExpectedConditions.visibilityOf(el));
            logger.info("Done");
            return el;

        } catch (Exception e) {
            logger.info("Waiting failed element visibility failed " + e.getMessage());
        }
        return null;
    }


    /**
     * Wait until all elements are visible
     *  @param locator By <p>Locator to wait for</p>
     * @param seconds int <p>Number of seconds to wait for</p>
     * @return
     */
    public  List<WebElement> waitUntilVisibilityOfAllElements(By locator, int seconds) {
        try {
            logger.info("Waiting for the visibility of all element : " + locator);
            new WebDriverWait(driver, seconds).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
            return driver.findElements(locator);
        } catch (Exception e) {
            logger.error("Error in visibility of all elements" + e.getMessage());
        }
        return null;
    }

    /**
     * Wait until list of elements are visible
     *
     * @param locator List<WebElement> <p>Locator to wait for</p>
     * @param seconds int <p>Number of seconds to wait for</p>
     */
    public  void waitUntilVisibilityOfAllElements(List<WebElement> locator, int seconds) {
        try {
            logger.info("Waiting for the visibility of all elements : " + locator);
            new WebDriverWait(driver, seconds).until(ExpectedConditions.visibilityOfAllElements(locator));
        } catch (Exception e) {
            logger.error("Error in visibility of all elements" + e.getMessage());
        }
    }

    /**
     * Wait until element is invisible
     *
     * @param by      By <p>Locator to wait for</p>
     */
    public  void waitUntilDisappear(By by) {
        waitUntilDisappear(by,waitTime);
    }

    public  void waitUntilDisappear(By by, int seconds) {
        try {
            logger.info("Waiting for the invisibility of element : " + by);
            new WebDriverWait(driver, seconds)
                    .until(ExpectedConditions.invisibilityOfElementLocated(by));
        } catch (Exception e) {
            logger.error("Error in invisibility of an element" + e.getMessage());
        }
    }


    public  void numberOfElementsToBeLessThan(By by,int count, int seconds) {
        try {
            logger.info("Waiting for the invisibility of element : " + by);
            new WebDriverWait(driver, seconds)
                    .until(ExpectedConditions.numberOfElementsToBeLessThan(by,count));
        } catch (Exception e) {
            logger.error("Error in invisibility of an element" + e.getMessage());
        }
    }


    /**
     * Wait until element is invisible
     *
     * @param locator WebElement <p>Locator to wait for</p>
     * @param seconds int <p>Number of seconds to wait for</p>
     */
    public  void waitUntilDisappear(WebElement locator, int seconds) {
        try {
            for (int i = 0; i < seconds * 100; i++) {
                 timeout(100);
                if (!new ElementHelper(this.driver).isElementDisplayed(locator))
                    break;
            }
        } catch (Exception e) {
            logger.error("Error in waiting for invisibility or absence of an element" + e.getMessage());
        }
    }
    public  void waitUntilDisappear(WebElement locator) {
        waitUntilDisappear(locator,waitTime);
    }


    /**
     * Wait until element text chnages to given text
     *
     * @param locator WebElement <p>Locator to wait for</p>
     * @param seconds int <p>Number of seconds to wait for</p>
     */
    public  void waitUntilTextToBe(WebElement locator,String text, int seconds) {
        try {
            logger.info("Waiting for the text of element " + locator+" to be "+text);
            new WebDriverWait(driver, seconds)
                    .until(ExpectedConditions.textToBePresentInElement(locator,text));
        } catch (Exception e) {
            logger.error("Error in waiting for the text of element" + e.getMessage());
        }
    }


    /**
     * Wait until all elements are invisible
     *
     * @param locator WebElement <p>Locator to wait for</p>
     * @param seconds int <p>Number of seconds to wait for</p>
     */


     void waitUntilInVisibilityOfAllElements(List<WebElement> locator, int seconds) {
        try {
            logger.info("Waiting for the invisibility of list of elements : " + locator);
            new WebDriverWait(driver, seconds).until(ExpectedConditions.invisibilityOfAllElements(locator));
        } catch (Exception e) {
            logger.error("Error in invisibility of all elements" + e.getMessage());
        }
    }

    /**
     * Wait until element is present in DOM
     *
     * @param locator By <p>Locator to wait for</p>
     * @param seconds int <p>Number of seconds to wait for</p>
     */
    public  void waitUntilElementPresence(By locator, int seconds) {
        try {
            logger.info("Waiting for presence of an element : " + locator);
            new WebDriverWait(driver, seconds).until(ExpectedConditions.presenceOfElementLocated(locator));
        } catch (Exception e) {
            logger.info("Error in presence of an element" + e.getMessage());
        }
    }
    public  void waitUntilElementPresence(By locator) {
        waitUntilElementPresence(locator,waitTime);
    }

    /**
     * Wait until all elements are present in DOM
     *
     * @param locator By <p>Locator to wait for</p>
     * @param seconds int <p>Number of seconds to wait for</p>
     */
    public  void waitUntilAllElementPresence(By locator, int seconds) {
        try {
            logger.info("Waiting for presence of all elements : " + locator);
            new WebDriverWait(driver, seconds).until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
        } catch (Exception e) {
            logger.info("Error in presence of all elements" + e.getMessage());
        }
    }

    /**
     * Wait until element is clickable
     *
     * @param locator By <p>Locator to wait for</p>
     * @param seconds int <p>Number of seconds to wait for</p>
     * @return
     */
    public  void waitUntilElementClickable(By locator, int seconds) {
        try {
            logger.info("Waiting for an element to be clickable : " + locator);
            new WebDriverWait(driver, waitTime).until(ExpectedConditions.elementToBeClickable(locator));

        } catch (Exception e) {
            logger.info("Error in waiting for an element to be clickable" + e.getMessage());
        }

    }

    public  void waitUntilElementClickable(By locator) {
        waitUntilElementClickable(locator,waitTime);
    }

    /**
     * Wait until element is clickable
     *
     * @param locator, Seconds
     */
    public  void waitUntilElementClickable(WebElement locator, int seconds) {
        try {
            logger.info("Waiting for an element to be clickable : " + locator);
            new WebDriverWait(driver, seconds).until(ExpectedConditions.elementToBeClickable(locator));
        } catch (Exception e) {
            logger.info("Error in waiting for an element to be clickable" + e.getMessage());
        }
    }

    /**
     * Wait until element is clickable
     *
     * @param locator, Seconds
     */
    public  void waitUntilElementClickable(WebElement locator) {
        waitUntilElementClickable(locator,waitTime);
    }

    /**
     * Wait until title of the web page contains as given string input
     *
     * @param title   String <p>Title of the web page</p>
     * @param seconds int <p>Number of seconds to wait for</p>
     */
    public  void waitUntilTitleContains(String title, int seconds) {
        try {
            logger.info("Waiting for title to be : " + title);
            new WebDriverWait(driver, seconds).until(ExpectedConditions.titleContains(title));
        } catch (Exception e) {
            logger.info("Error in waiting for the title" + e.getMessage());
        }
    }

    /**
     * Wait until title of the web page contains as given string input
     *
     * @param urlFraction String <p>Fraction of Url of the web page</p>
     * @param seconds     int <p>Number of seconds to wait for</p>
     */
    public  void waitUntilUrlContains(String urlFraction, int seconds) {
        try {
            logger.info("Waiting for title to be : " + urlFraction);
            new WebDriverWait(driver, seconds).until(ExpectedConditions.urlContains(urlFraction));
        } catch (Exception e) {
            logger.info("Error in waiting for the url" + e.getMessage());
        }
    }

    /**
     * Wait until alert is present
     */
    public  void waitUntilAlertPresence() {
        try {
            logger.info("Waiting for alert to appear");
            new WebDriverWait(driver, waitTime).until(ExpectedConditions.alertIsPresent());
        } catch (Exception e) {
            logger.info("Error in waiting for the alert to appear" + e.getMessage());
        }
    }

    /**
     * Below method waits until JS of the page is loaded
     *
     * @param seconds int <p>Number of seconds to wait for</p>
     * @return boolean
     */
    public  boolean waitForJStoLoad(int seconds) {
        try {
            logger.info("Waiting for JS of page to load");
            final JavascriptExecutor jse = (JavascriptExecutor) driver;
            WebDriverWait wait = new WebDriverWait(driver, seconds);

            // wait for jQuery to load
            ExpectedCondition<Boolean> jQueryLoad = driver -> {
                try {
                    return ((Long) jse.executeScript("return jQuery.active") == 0);
                } catch (Exception e) {
                    return true;
                }
            };
            // wait for Javascript to load
            ExpectedCondition<Boolean> jsLoad = driver -> jse.executeScript("return document.readyState")
                    .toString().equals("complete");
            return wait.until(jQueryLoad) && wait.until(jsLoad);
        } catch (Exception e) {
            logger.error("Error in JS load");
        }
        return false;
    }

    /**
     * Wait until no of tabs to be
     */
    public  void waitUntilNoOfTabsTobe(int tab) {
        try {
            new WebDriverWait(driver,waitTime).until(ExpectedConditions.numberOfWindowsToBe(tab));
        } catch (Exception e) {
            logger.info("Error in waiting for the title" + e.getMessage());
        }
    }
}