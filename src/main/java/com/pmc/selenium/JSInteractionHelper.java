package com.pmc.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class JSInteractionHelper {
    private  final Logger logger = LogManager.getLogger(JSInteractionHelper.class.getName());

    WebDriver driver;
    WaitHelper waitHelper;
    public JSInteractionHelper(WebDriver driver)
    {
        this.driver=driver;
        waitHelper=new WaitHelper(driver);
    }

    /**
     * Below method highlights element using JS
     *
     * @param element WebElement <p>Locator to be clicked on</p>
     */
    public  void highLightElement(WebElement element) {
        try {
            final JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("arguments[0].setAttribute('style', 'border: 2px solid red ;');", element);
        } catch (Exception e) {
            logger.info("Error in highlighting element using javascript: " + e.getMessage());
        }
    }

    /**
     * Below method highlights element using JS
     *
     * @param element WebElement <p>Locator to be clicked on</p>
     */
    public  void highLightElement(WebElement element, String color) {
        try {
            final JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("arguments[0].setAttribute('style', 'border: 2px solid "+color+" ;');", element);
        } catch (Exception e) {
            logger.info("Error in highlighting element using javascript: " + e.getMessage());
        }
    }

    /**
     * Below method helps us click on an element with Javascript
     *
     * @param element WebElement <p>Locator to be clicked on</p>
     */
    public  void clickWithJS(WebElement element) {
            final JavascriptExecutor jse = (JavascriptExecutor) driver;
            waitHelper.waitForJStoLoad(5);
            new JSInteractionHelper(driver).highLightElement(element);
            jse.executeScript("arguments[0].click();", element);
    }

    /**
     * Below method helps us click on an element with Javascript
     *
     * @param by By <p>Locator to be clicked on</p>
     */
    public  void clickWithJS(By by) {
            final JavascriptExecutor jse = (JavascriptExecutor) driver;
        waitHelper.waitUntilElementPresence(by);
            WebElement el= driver.findElement(by);
        waitHelper.waitUntilElementClickable(el);
            new JSInteractionHelper(driver).highLightElement(el);
            jse.executeScript("arguments[0].click();", el);
    }

    /**
     * To enter text using JS
     *
     * @param element WebElement <p>Locator to send text to</p>
     * @param text    Text to be entered
     */
    public  void enterTextWithJS(WebElement element, String text) {
        final JavascriptExecutor jse = (JavascriptExecutor) driver;
        new JSInteractionHelper(driver).highLightElement(element);
        waitHelper.timeout(200);
        jse.executeScript("arguments[0].click();", element);
        jse.executeScript("arguments[0].value='" + text + "';", element);
        logger.info("Entered text to an element using JavaScript : " + text);
    }

    /**
     * To set Attribute Value using JS
     *
     * @param element WebElement <p>Locator to send text to</p>
     * @param text    Text to be entered
     * @param text    Text to be entered
     */

    public  void setElementAttributeValue(WebElement element, String Attribute, String text) {
            final JavascriptExecutor jse = (JavascriptExecutor) driver;
            new JSInteractionHelper(driver).highLightElement(element);
            jse.executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",element, Attribute, text);
            logger.info("Entered text to an element using JavaScript : "+text);
    }
}