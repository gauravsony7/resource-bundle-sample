package com.pmc.selenium;

import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import io.github.bonigarcia.wdm.managers.FirefoxDriverManager;
import io.github.bonigarcia.wdm.managers.InternetExplorerDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Below method contains all the methods to initiate different browsers with different properties
 */
public class BrowserInitiationHelper {
    private  final Logger logger = LogManager.getLogger(BrowserInitiationHelper.class.getName());
    private  FirefoxOptions firefoxOptions;
    private  FirefoxProfile firefoxProfile;
    private  ChromeOptions chromeOptions;
    private  Map<String, Object> chromePrefs;

    WebDriver driver = null;

     {
        chromeOptions = new ChromeOptions();
        firefoxOptions = new FirefoxOptions();
        firefoxProfile = new FirefoxProfile();
    }

    /**
     * Below method initializes the driver based on browser name provided
     *
     * @param browser String <p>Browser name- Chrome/Firefox/IE</p>
     * @return WebDriver
     */
    public WebDriver getBrowser(String browser) throws MalformedURLException {

        switch (browser) {
            case "ie":
                InternetExplorerDriverManager.iedriver().setup();
                driver = new InternetExplorerDriver();
                break;
            case "chrome":
                ChromeDriverManager.chromedriver().setup();
                HashMap<String, Object> chromePref = new HashMap<>();
                chromePref.put("download.default_directory", System.getProperty("user.dir") + "\\testData");
                chromeOptions.setExperimentalOption("prefs", chromePref);
                chromeOptions.addArguments("--incognito");
                driver = new ChromeDriver(chromeOptions);

              //   desiredBrowser = new RemoteWebDriver(new URL("http://10.62.3.14:4444"),chromeOptions);

                break;
            case "firefox":
                FirefoxDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver(firefoxOptions);
                break;
            default:
                break;
        }
        assert driver != null;
        logger.info("Driver initialized successfully, Size " + driver.manage().window().getSize());
        driver.manage().window().maximize();
        driver.manage().timeouts().setScriptTimeout(10000, TimeUnit.MILLISECONDS);
        logger.info("Browser maximized");
        return driver;
    }

    /**
     * Below method opens browser in an Incognito mode
     *
     * @param browser String <p>Browser name- Chrome/Firefox</p>
     */
    public  void setIncognito(String browser) {
        if (browser.equalsIgnoreCase("chrome"))
            chromeOptions.addArguments("--incognito");
        else if (browser.equalsIgnoreCase("firefox"))
            firefoxOptions.addPreference("browser.privatebrowsing.autostart", true);
    }

    /**
     * Below method disables Popup-blocking
     *
     * @param browser String <p>Browser name- Chrome/Firefox</p>
     */
    public  void disablePopUpBlock(String browser) {
        if (browser.equalsIgnoreCase("chrome"))
            chromeOptions.addArguments("--disable-popup-blocking");
        else if (browser.equalsIgnoreCase("firefox"))
            firefoxOptions.addPreference("dom.disable_beforeunload", true);
    }

    /**
     * Below method opens browser in a Headless mode
     *
     * @param browser String <p>Browser name- Chrome/Firefox</p>
     */
    public  void setHeadless(String browser) {
        if (browser.equalsIgnoreCase("chrome"))
            chromeOptions.setHeadless(true);
        else if (browser.equalsIgnoreCase("firefox"))
            firefoxOptions.setHeadless(true);
    }

    /**
     * Below method sets download directory for a browser instance
     *
     * @param browser         String <p>Browser name- Chrome/Firefox</p>
     * @param downloadDirPath String <p>Download directory path</p>
     */
    public  void setDownloadDirectory(String browser, String downloadDirPath) {
        if (browser.equalsIgnoreCase("chrome"))
            chromePrefs.put("download.default_directory", downloadDirPath);
        else if (browser.equalsIgnoreCase("firefox")) {
            firefoxProfile.setPreference("browser.download.folderList", 2);
            firefoxProfile.setPreference("browser.download.dir", downloadDirPath);
            firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/java-archive, application/x-msexcel,application/excel,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml,application/vnd.microsoft.portable-executable");
            firefoxOptions.setProfile(firefoxProfile);
        }
    }

    /**
     * To delete all the cookies before starting browser
     */
    public void deleteAllCookies() {
        driver.manage().deleteAllCookies();
        final JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.sessionStorage.clear();");
    }

    /**
     * Below method closes the browser instance
     */
    public  void closeBrowser() {
        try {
            if (driver != null) {
                driver.quit();
                logger.info("Browser closed");
            }
        } catch (WebDriverException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }
}


