package com.pmc.selenium;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

public class ElementHelper {
    private  final Logger logger = LogManager.getLogger(ElementHelper.class.getName());
    WebDriver driver;
    WaitHelper waitHelper;
    JSInteractionHelper jsHelper;
    public ElementHelper(WebDriver driver)
    {
        this.driver=driver;
        waitHelper=new WaitHelper(driver);
        jsHelper=new JSInteractionHelper(driver);
    }

    /**
     * Below method checks if a particular element is present in DOM or Not
     *
     * @param by By <p>Locator of the element</p>
     * @return boolean <p>true if element is present, false otherwise</p>
     */
    public  boolean isElementPresent(By by) {
        return driver.findElements(by).size() > 0;
    }

    /**
     * To check whether an element is displayed on DOM or not
     *
     * @param by By <p>Locator of an element</p>
     * @return boolean <p>true if element is displayed, false otherwise</p>
     */
    public  boolean isElementDisplayed(By by) {
        return isElementDisplayed(by,0);
    }

    /**
     * To check whether an element is displayed on DOM or not
     * @return boolean <p>true if element is displayed, false otherwise</p>
     */
    public  boolean isElementDisplayed(WebElement ele) {
        return isElementDisplayed(ele,0);
    }

    public  boolean isElementDisplayed(By by,int waitSeconds) {
        boolean isDisplayed;
        try {
            waitHelper.waitUntilVisibilityOf(by,waitSeconds);
            isDisplayed = driver.findElement(by).isDisplayed();
            if (isDisplayed)
                jsHelper.highLightElement(driver.findElement(by), "Blue");

            return isDisplayed;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    /**
     * To check whether an element is displayed on DOM or not
     * @return boolean <p>true if element is displayed, false otherwise</p>
     */
    public  boolean isElementDisplayed(WebElement ele,int waitSeconds) {
        boolean isDisplayed;
        try {
            waitHelper.waitUntilVisibilityOf(ele,waitSeconds);
            isDisplayed = ele.isDisplayed();
            jsHelper.highLightElement(ele, "Blue");
            return isDisplayed;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    /**
     * Below method clicks on an element
     *
     * @param element WebElement <p>Locator of an element</p>
     */
    public  void clickElement(WebElement element) {
        logger.info("Clicking on an element : " + element);
        waitHelper.waitUntilElementClickable(element);
        try {
            new ActionsHelper(driver).moveToElement(element, "Red");
        } catch (Exception e) {
        }
        waitHelper.timeout(200);
        element.click();
    }


    /**
     * Below method clicks on an element
     *
     * @param element WebElement <p>Locator of an element</p>
     */
    public  void clickToDisappear(WebElement element) {
        logger.info("Clicking on an element : " + element);
        clickElement(element);
        waitHelper.waitUntilDisappear(element);
    }

    /**
     * Below method clicks on an element
     *
     * @param element WebElement <p>Locator of an element</p>
     */
    public  void clearTextElement(WebElement element) {
        logger.info("Clicking on an element : " + element);
        waitHelper.waitUntilElementClickable(element);
        jsHelper.highLightElement(element);
        element.clear();
    }

    /**
     * Below method click on an element
     *
     * @param by By <p>Locator of an element</p>
     */
    public  void clickElement(By by) {
        logger.info("Clicking on an element : " + by);
        waitHelper.waitUntilElementPresence(by);
        WebElement element = driver.findElement(by);
        waitHelper.waitUntilElementClickable(element);
        try {
            new ActionsHelper(driver).moveToElement(element, "Red");
        } catch (Exception e) {
        }
        waitHelper.timeout(200);
        element.click();
    }

    /**
     * Below method enters text to an element
     *
     * @param element WebElement <p>Locator of an element</p>
     * @param text    String <p>Text to be entered</p>
     */
    public  void enterText(WebElement element, String text) {
       enterText(element,text,false);
    }

    /**
     * Below method enters text to an element
     *
     * @param element WebElement <p>Locator of an element</p>
     * @param text    String <p>Text to be entered</p>
     */
    public  void enterText(WebElement element, String text,boolean clearWithAction) {
        logger.info("Entering text in an element : " + element);
        clickElement(element);
        if(clearWithAction)
            element.sendKeys(Keys.chord(Keys.CONTROL,"a",Keys.DELETE));
        else
            element.clear();

        waitHelper.timeout(100);
        element.sendKeys(text);
    }

    /**
     * Below method enters text to an element
     *
     * @param by   By <p>Locator of an element</p>
     * @param text String <p>Text to be entered</p>
     */
    public  void enterText(By by, String text) {
        logger.info("Entering text in an element : " + by);
        waitHelper.waitUntilElementPresence(by);
            WebElement element = driver.findElement(by);
        waitHelper.waitUntilElementClickable(element);
        jsHelper.highLightElement(element);
            element.clear();
            element.sendKeys(text);
    }

    /**
     * Below method gets text from an element
     *
     * @param element WebElement <p>Locator of an element</p>
     * @return String
     */
    public  String getElementText(WebElement element) {

       String elText=getElementText(element,true);
       System.out.println("Element text is : "+elText);
        return elText;
    }

    /**
     * Below method gets text from an element
     *
     * @param element WebElement <p>Locator of an element</p>
     * @return String
     */
    public  String getElementText(WebElement element, boolean wait) {
        String value = null;
            logger.info("Getting text from element : " + element);
            if (wait)
                waitHelper.waitUntilElementClickable(element);
            jsHelper.highLightElement(element);
            value = element.getText();
        return value;
    }

    /**
     * Below method gets text from an element
     *
     * @param by By <p>Locator of an element</p>
     * @return String
     */
    public  String getElementText(By by) {
        String value = null;
        logger.info("Getting text from element : " + by);
        waitHelper.waitUntilElementPresence(by);
            WebElement element = driver.findElement(by);
        jsHelper.highLightElement(element);
            value = element.getText();
            logger.info("Text for element is  : " + value);
        return value;
    }

    /**
     * Below method gets attribute value of an element
     *
     * @param element       WebElement <p>Locator of an element</p>
     * @param attributeName String <p>Name of an attribute</p>
     * @return String
     */
    public  String getAttributeValue(WebElement element, String attributeName) {
        String value = null;
        logger.info("Getting text from element : " + element);
        waitHelper.waitUntilElementClickable(element);
        jsHelper.highLightElement(element);
            value = element.getAttribute(attributeName);
        return value;
    }
}