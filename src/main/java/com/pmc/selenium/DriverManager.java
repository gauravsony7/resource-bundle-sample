package com.pmc.selenium;

import org.openqa.selenium.WebDriver;

/**
 * Below class manages thread safe driver object at every instance
 */
public class DriverManager {
    private ThreadLocal<WebDriver> webDriver = new ThreadLocal<>();

    /**
     * Below method gets the web driver instance
     *
     * @return webDriver <p>Web driver instance</p>
     */
    public WebDriver getDriver() {
        return webDriver.get();
    }

    /**
     * Below method sets the web driver instance
     *
     * @param driver <p>Created webDriver instance</p>
     */
    public void setDriver(WebDriver driver) {
        webDriver.set(driver);
    }
}