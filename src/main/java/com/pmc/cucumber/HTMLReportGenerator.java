package com.pmc.cucumber;

import com.pmc.utils.DateTimeUtil;
import com.pmc.utils.PropertyUtil;
import com.pmc.utils.SystemUtil;
import io.cucumber.java.Scenario;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.TestRunner;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.sql.Timestamp;
import java.util.List;
import java.util.*;
import java.util.Map.Entry;

/**
 * Below class generates cucumber html report and opens a generated report in browser
 */
public class HTMLReportGenerator {

    private static final PropertyUtil property =  new PropertyUtil(SystemUtil.getUserDirectory()+"/configuration/config.properties");
    private static final String JSON_REPORT_PATH = SystemUtil.getUserDirectory() + "/target/report";
    private static final String HTML_REPORT_PATH = "/cucumber-html-reports/overview-features.html";
    private static final String REPORT_DIRECTORY = "reports";
    private static final String HTML_REPORT_LOCATION = SystemUtil.getUserDirectory() + "/reports/html/Report_";
    private static final Logger logger = LogManager.getLogger(HTMLReportGenerator.class.getName());

    public static void main(String args[]) {
               generateReport();
    }

    /**
     * To generate html reports and open if showInBrowser is yes
     */
    public static void generateReport() {
        String currentReportLoc = HTML_REPORT_LOCATION + DateTimeUtil.getCurrentTimeStampIST().replaceAll("[/.,: ]", "_").toUpperCase();
        System.out.println("Report location : " + currentReportLoc);
        logger.info("Report location : " + currentReportLoc);
        List<String> jsonFiles=generateCucumberReport(currentReportLoc, getReportMetaData());
        OpenReport(currentReportLoc);

        copyJsonFileWithHTML(JSON_REPORT_PATH,currentReportLoc);
    }

    /**
     * To get report metadata
     *
     * @return Map<String, String> <p>Returns key value pairs of metadata to be displayed on report</p>
     */
    private static Map<String, String> getReportMetaData() {
        Map<String, String> metaData = new HashMap<>();
        metaData.put("Application URL : ", property.getProperty("app-url"));
        return metaData;
    }

    /**
     * To open html report in browser
     *
     * @param reportLoc String <p>The location of the report generated</p>
     */
    private static void OpenReport(String reportLoc) {
        try {
            if (property.getProperty("showReportInBrowser").equalsIgnoreCase("yes")) {
                File htmlFile = new File(reportLoc + HTML_REPORT_PATH);
                if (htmlFile.exists()) {
                    Desktop.getDesktop().browse(htmlFile.toURI());
                } else {
                    throw new NoSuchFileException("File does not exist at : " + reportLoc + HTML_REPORT_PATH);
                }
            }
        } catch (Exception e1) {
            logger.error(e1.getMessage(), e1);
        }
    }

    /**
     * To generate cucumber report
     *  @param reportDirectory String <p>Json Report directory</p>
     * @param reportMetadata  Map<String, String> <p>Metadata for report</p>
     * @return
     */
    private static List<String> generateCucumberReport(String reportDirectory, Map<String, String> reportMetadata) {

        File reportOutputDirectory = new File(reportDirectory);
        File directory = new File(REPORT_DIRECTORY);
        File jsonFile = new File(HTMLReportGenerator.JSON_REPORT_PATH);
        List<String> jsonFiles = new ArrayList<>();

        for (File cucumberFile : Objects.requireNonNull(jsonFile.listFiles())) {
            if (cucumberFile.isFile()) {
                if (FilenameUtils.getExtension(cucumberFile.getName()).equalsIgnoreCase("json")) {
                    jsonFiles.add(cucumberFile.getAbsolutePath());
                    logger.info(cucumberFile.getAbsolutePath());
                }
            }
        }

        if (!directory.exists()) {
            directory.mkdir();
        }

        Configuration configuration = new Configuration(reportOutputDirectory, property.getProperty("project-name"));
        for (Entry<String, String> entry : reportMetadata.entrySet()) {
            configuration.addClassifications(entry.getKey(), entry.getValue());
        }
        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        reportBuilder.generateReports();
        return jsonFiles;
    }

    /**
     * Adds String in json report specific to current scenario
     */
    public static void addDataToReport(Scenario sc,String msg) {
        System.out.println(msg);
        sc.log(msg);
    }


    /**
     * Adds List<String> in json report specific to current scenario
     */
    public static void addDataToReport(Scenario sc,List<String> msg) {
        sc.log(String.valueOf(msg));
    }

    /**
     * Below method adds a document to cucumber report
     *
     * @param filePath String <p>location of the file</p>
     * @throws IOException
     */
    public static void attachDocToReport(Scenario sc,String filePath) throws IOException {
        if (filePath != null)
            sc.attach(FileUtils.readFileToByteArray(new File(filePath)), "application/pdf", "Order Confirmation Receipt");
        else
            throw new IllegalArgumentException("File path missing");
    }

    public static void  copyJsonFileWithHTML(String JsnDir,String HTMLDir)
    {
        try {
            File srcDir = new File(JsnDir);
            File trgDir = new File(HTMLDir);
            FileUtils.copyDirectory(srcDir, trgDir);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}