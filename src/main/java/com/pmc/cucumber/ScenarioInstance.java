package com.pmc.cucumber;

import io.cucumber.java.Scenario;
import org.openqa.selenium.WebDriver;

/**
 * Below class keeps a Scenario instance
 */
public class ScenarioInstance {

    private ThreadLocal<Scenario> scenario = new ThreadLocal<>();

    /**
     * Below method gets the Scenario instance
     *
     * @return scenario Scenario <p>instance</p>
     */
    public Scenario getScenario() {
        return scenario.get();
    }

    /**
     * Below method sets the Scenario instance
     */
    public void setScenario(Scenario s) {
        scenario.set(s);
    }
}
